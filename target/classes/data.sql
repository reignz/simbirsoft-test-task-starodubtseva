CREATE DATABASE "test-task-db"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE TABLE public.sites
(
    id integer NOT NULL DEFAULT nextval('sites_id_seq'::regclass),
    url text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT sites_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.sites
    OWNER to postgres;

CREATE TABLE public.words
(
    id integer NOT NULL DEFAULT nextval('words_id_seq'::regclass),
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    frequency bigint NOT NULL,
    site_id integer NOT NULL DEFAULT nextval('words_site_id_seq'::regclass),
    CONSTRAINT words_pkey PRIMARY KEY (id),
    CONSTRAINT sites_fk FOREIGN KEY (site_id)
        REFERENCES public.sites (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.words
    OWNER to postgres;