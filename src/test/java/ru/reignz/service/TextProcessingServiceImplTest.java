package ru.reignz.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import ru.reignz.entity.Word;
import ru.reignz.service.impl.TextProcessingServiceImpl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(BlockJUnit4ClassRunner.class)
public class TextProcessingServiceImplTest {

    private static final String SPACE_DELIMITER = " ";
    private static final String TEST_STRING = "С!!! 2001.года ]IT company     that cares";
    private static final String RESULT_STRING = "С 2001 года IT company that cares";
    private static final TextProcesssingService textProcessingService = new TextProcessingServiceImpl();

    @Test
    public void getWordsTest() {
        List<Word> result = textProcessingService.getWords(TEST_STRING);
        List<String> resultList = result.stream()
                .map(Word::getWord)
                .sorted()
                .collect(Collectors.toList());
        List<String> resultStringList = Arrays.stream(RESULT_STRING.toUpperCase().split(SPACE_DELIMITER))
                .sorted()
                .collect(Collectors.toList());
        Assert.assertEquals(resultList, resultStringList);
    }

    @Test
    public void removeDelimitersTest() {
        String resultString = textProcessingService.removeDelimiters(TEST_STRING);
        Assert.assertEquals(resultString, RESULT_STRING);
    }

}
