package ru.reignz.dao;

import java.util.List;

public interface DaoService<T> {

    /**
     * saves entity to database
     */
    boolean add(T entity);

    /**
     * saves list of entities to database
     */
    boolean addAll(List<T> entityList);

}
