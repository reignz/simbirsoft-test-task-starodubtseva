package ru.reignz.dao.impl;

import ru.reignz.constants.Database;
import ru.reignz.constants.LoggerMessages;
import ru.reignz.dao.DaoService;
import ru.reignz.entity.Site;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class SiteDaoService implements DaoService<Site> {

    private static final Logger logger = LoggerFactory.getLogger(SiteDaoService.class);

    @Override
    public boolean add(Site site) {
        try (Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD)) {

            PreparedStatement statement = connection.prepareStatement("INSERT INTO sites VALUES (?, ?)");
            statement.setLong(1, site.getId());
            statement.setString(2, site.getUrl());

            return statement.execute();
        } catch (SQLException e) {
            logger.error(LoggerMessages.DataBase.UNABLE_TO_SAVE + site.toString());
            return false;
        }
    }

    @Override
    public boolean addAll(List<Site> entityList) {
        try (Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD)) {
            for (Site site : entityList) {
                PreparedStatement statement = connection.prepareStatement("INSERT INTO sites VALUES (?, ?)");
                statement.setLong(1, site.getId());
                statement.setString(2, site.getUrl());
                statement.execute();
            }
            return true;
        } catch (SQLException e) {
            logger.error(LoggerMessages.DataBase.UNABLE_TO_SAVE + entityList);
            return false;
        }
    }
}
