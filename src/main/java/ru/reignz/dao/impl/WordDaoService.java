package ru.reignz.dao.impl;

import ru.reignz.constants.Database;
import ru.reignz.constants.LoggerMessages;
import ru.reignz.dao.DaoService;
import ru.reignz.entity.Word;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class WordDaoService implements DaoService<Word> {

    private static final Logger logger = LoggerFactory.getLogger(WordDaoService.class);

    @Override
    public boolean add(Word word) {
        try (Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD)) {

            PreparedStatement statement = connection.prepareStatement("INSERT INTO words VALUES (?, ?, ?, ?)");
            statement.setLong(1, word.getId());
            statement.setString(2, word.getWord());
            statement.setLong(3, word.getFrequency());
            statement.setLong(4, word.getSiteId());

            return statement.execute();
        } catch (SQLException e) {
            logger.error(LoggerMessages.DataBase.UNABLE_TO_SAVE + word.toString());
            return false;
        }
    }

    @Override
    public boolean addAll(List<Word> entityList) {
        try (Connection connection = DriverManager.getConnection(Database.URL, Database.LOGIN, Database.PASSWORD)) {
            for (Word word : entityList) {
                PreparedStatement statement = connection.prepareStatement("INSERT INTO words VALUES (?, ?, ?, ?)");
                statement.setLong(1, word.getId());
                statement.setString(2, word.getWord());
                statement.setLong(3, word.getFrequency());
                statement.setLong(4, word.getSiteId());

                statement.execute();
            }
            return true;
        } catch (SQLException e) {
            logger.error(LoggerMessages.DataBase.UNABLE_TO_SAVE + entityList);
            return false;
        }    }
}
