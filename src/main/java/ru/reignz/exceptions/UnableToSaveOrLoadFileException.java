package ru.reignz.exceptions;

public class UnableToSaveOrLoadFileException extends RuntimeException {

    public UnableToSaveOrLoadFileException(String message) {
        super(message);
    }
}
