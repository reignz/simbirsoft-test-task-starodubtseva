package ru.reignz.exceptions;

public class GetDocumentException extends RuntimeException {

    public GetDocumentException(String message) {
        super(message);
    }
}
