package ru.reignz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.reignz.constants.Filepath;
import ru.reignz.dao.DaoService;
import ru.reignz.dao.impl.SiteDaoService;
import ru.reignz.dao.impl.WordDaoService;
import ru.reignz.entity.Site;
import ru.reignz.entity.Word;
import ru.reignz.service.FileOperatingService;
import ru.reignz.service.JsoupService;
import ru.reignz.service.TextProcesssingService;
import ru.reignz.service.impl.FileOperatingServiceImpl;
import ru.reignz.service.impl.JsoupServiceImpl;
import ru.reignz.service.impl.TextProcessingServiceImpl;

import java.util.List;
import java.util.Scanner;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    private static final JsoupService jsoupService = new JsoupServiceImpl();
    private static final FileOperatingService fileOperatingService = new FileOperatingServiceImpl();
    private static final DaoService<Site> siteDaoService = new SiteDaoService();
    private static final DaoService<Word> wordDaoService = new WordDaoService();
    private static final TextProcesssingService tps = new TextProcessingServiceImpl();


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String url = in.nextLine();
        boolean isSaved = fileOperatingService.download(url, Filepath.HTML_FOLDER + url.hashCode() + ".html");

        String text = jsoupService.getText(Filepath.HTML_FOLDER + url.hashCode() + ".html", url, isSaved);

        List<Word> words = tps.getWords(text);

        Site site = new Site(Math.abs(url.hashCode()), url);
        siteDaoService.add(site);
        long i = 0L;
        for (Word word : words) {
            logger.info(word.getWord() + " - " + word.getFrequency());
        }
        for (Word word : words) {
            word.setId(i++);
            word.setSiteId(site.getId());
        }
        wordDaoService.addAll(words);
    }

}
