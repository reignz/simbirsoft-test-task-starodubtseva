package ru.reignz.service;

import ru.reignz.entity.Word;

import java.util.List;

public interface TextProcesssingService {

    /**
     * @param text chosen text
     * @return words from text with frequency
     */
    List<Word> getWords(String text);

    /**
     * Removes delimiters from chosen string
     */
    String removeDelimiters(String str);
}
