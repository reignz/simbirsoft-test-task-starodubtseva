package ru.reignz.service;

public interface FileOperatingService {

    /**
     * Downloads html from chosen url
     *
     * @param url websites url
     * @param path path to save file
     * @return is download valid
     */
    boolean download(String url, String path);
}
