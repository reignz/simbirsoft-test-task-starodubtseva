package ru.reignz.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.reignz.entity.Word;
import ru.reignz.service.TextProcesssingService;

import java.util.*;
import java.util.stream.Collectors;

public class TextProcessingServiceImpl implements TextProcesssingService {

    private static final String DELIMITERS_REGEX = "[,.!?;:\\[\\]\"()\n\r\t]";
    private static final String MULTIPLE_SPACE_REGEX = "\\s+";
    private static final String SPACE = " ";

    private static final Logger logger = LoggerFactory.getLogger(TextProcessingServiceImpl.class);

    @Override
    public List<Word> getWords(String text) {

        text = removeDelimiters(text).toUpperCase();
        Map<String, Long> wordsCount = new HashMap<>();
        for (String s : text.split(SPACE)) {
            wordsCount.putIfAbsent(s, 0L);
            wordsCount.put(s, wordsCount.get(s) + 1);
        }

        wordsCount = wordsCount.entrySet().stream().
                sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).
                collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        List<Word> words = new ArrayList<>();
        for (Map.Entry<String, Long> entry : wordsCount.entrySet()) {
            words.add(new Word(entry.getKey(), entry.getValue()));
        }
        return words;
    }

    @Override
    public String removeDelimiters(String str) {
        str = str.replaceAll(DELIMITERS_REGEX, SPACE)
                .replaceAll(MULTIPLE_SPACE_REGEX, SPACE)
                .trim();
        logger.info(str);
        return str;
    }
}
