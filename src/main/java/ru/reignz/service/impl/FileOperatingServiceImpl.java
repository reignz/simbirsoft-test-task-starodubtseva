package ru.reignz.service.impl;

import ru.reignz.constants.LoggerMessages;
import ru.reignz.exceptions.UnableToSaveOrLoadFileException;
import ru.reignz.service.FileOperatingService;

import java.io.*;
import java.net.URL;

public class FileOperatingServiceImpl implements FileOperatingService {

    @Override
    public boolean download(String url, String path) {

        File file = new File(path);
        File parent = file.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new IllegalStateException("Couldn't create dir: " + parent);
        }

        boolean isSaved;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
             BufferedWriter writer = new BufferedWriter(new FileWriter(path));) {


            String line;
            while ((line = reader.readLine()) != null) {
                writer.write(line + "\n");
            }

            isSaved = true;
        } catch (IOException e) {
            throw new UnableToSaveOrLoadFileException(LoggerMessages.File.UNABLE_TO_GET_OR_SAVE_FILE);
        }
        return isSaved;
    }
}
