package ru.reignz.service.impl;

import ru.reignz.constants.LoggerMessages;
import ru.reignz.exceptions.GetDocumentException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import ru.reignz.service.JsoupService;

import java.io.File;
import java.io.IOException;

public class JsoupServiceImpl implements JsoupService {

    @Override
    public String getText(String filepath, String url, boolean isSaved) {
        Document document;
        if (isSaved) {
            document = getDocumentFromFile(filepath);
        } else {
            document = getDocumentByUrl(url);
        }
        return getText(document);
    }

    private Document getDocumentByUrl(String url) {
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            throw new GetDocumentException(LoggerMessages.Jsoup.UNABLE_TO_GER_DOCUMENT);
        }
    }

    private Document getDocumentFromFile(String filePath) {
        File file = new File(filePath);
        try {
            return Jsoup.parse(file, "UTF-8");
        } catch (IOException e) {
            throw new GetDocumentException(LoggerMessages.Jsoup.UNABLE_TO_GER_DOCUMENT);
        }
    }

    private String getText(Document doc) {
        return doc.body().text();
    }
}
