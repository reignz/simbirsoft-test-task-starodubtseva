package ru.reignz.service;

public interface JsoupService {

    /**
     * @param filepath path to chosen file
     * @param url url of site from where html was downloaded
     * @param isSaved isDownloadValid
     * @return text of html file
     */
    String getText(String filepath, String url, boolean isSaved);

}
