package ru.reignz.constants;

public interface Filepath {

    String RESOURCES = "src/main/resources/";
    String HTML_FOLDER = RESOURCES + "html/site";
}
