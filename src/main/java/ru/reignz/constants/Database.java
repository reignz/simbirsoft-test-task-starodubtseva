package ru.reignz.constants;

public interface Database {
    String URL = "jdbc:postgresql://localhost:5432/test-task-db";
    String LOGIN = "postgres";
    String PASSWORD = "postgres";
}
