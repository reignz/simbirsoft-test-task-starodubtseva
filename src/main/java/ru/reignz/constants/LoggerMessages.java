package ru.reignz.constants;

public interface LoggerMessages {

    interface DataBase {
        String UNABLE_TO_SAVE = "Unable to save ru.reignz.entity: ";
    }

    interface File {
        String UNABLE_TO_GET_OR_SAVE_FILE = "Unable to get or save file";
    }

    interface Jsoup {
        String UNABLE_TO_GER_DOCUMENT = "Unable to get document exception";
    }
}
