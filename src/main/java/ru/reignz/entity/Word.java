package ru.reignz.entity;

public class Word {

    //region Properties
    private Long id;
    private String word;
    private Long frequency;
    private Long siteId;
    //endregion

    //region Constructors
    public Word(String word, Long frequency) {
        this.word = word;
        this.frequency = frequency;
    }

    public Word(Long id, String word, Long frequency, Long siteId) {
        this.id = id;
        this.word = word;
        this.frequency = frequency;
        this.siteId = siteId;
    }
    //endregion

    //region Getters&Setters
    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Long getFrequency() {
        return frequency;
    }

    public void setFrequency(Long frequency) {
        this.frequency = frequency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }
    //endregion


    @Override
    public String toString() {
        return "Word{" +
                "id=" + id +
                ", word='" + word + '\'' +
                ", frequency=" + frequency +
                ", siteId=" + siteId +
                '}';
    }
}
